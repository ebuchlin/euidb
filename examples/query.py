from euidb.database import DataBase, EuiFile

d = DataBase()

# Using SQLAlchemy ORM: get EuiFile objects
l = d.session.query(EuiFile).filter_by(
        level='L1',
        naxis1=320,
        imgtype='LED image'
    )
print(list(l))

# Using SQL query: get table
l = d.engine.connect().execute("SELECT filename, xposure, gaincomb FROM eui_files WHERE level='L1' AND naxis1=320 AND imgtype='LED image'")
print(list(l))
