from euidb.database import DataBase

d = DataBase()
print(f'Before: {d.count_files()} files in database')
d.scan_dir()
print(f'After: {d.count_files()} files in database')
