import sqlalchemy as sq
from sqlalchemy.ext.declarative import declarative_base
import os.path
import glob
from datetime import datetime
from astropy.io import fits

import dateutil.parser # only needed for Python < 3.7

from . import parameters


def fromisoformat(s):
    if s is None:
        return None
    try:
        d = datetime.fromisoformat(s)
    except AttributeError:
        d = dateutil.parser.parse(s)
    return d

Base = declarative_base()

class EuiFile(Base):
    '''
    Metadata for an EUI file
    '''
    __tablename__ = 'eui_files'
    id = sq.Column(sq.Integer, primary_key=True)
    filename = sq.Column(sq.String, unique=True, nullable=False)
    filepath = sq.Column(sq.String, nullable=False)
    date = sq.Column(sq.DateTime, nullable=False)
    naxis1 = sq.Column(sq.Integer, nullable=False)
    naxis2 = sq.Column(sq.Integer, nullable=True)
    obt_beg = sq.Column(sq.Float, nullable=False)
    date_beg = sq.Column(sq.DateTime, nullable=True)
    level = sq.Column(sq.String, nullable=False)
    vers_sw = sq.Column(sq.String, nullable=False) # FK?
    version = sq.Column(sq.String, nullable=False)
    imgtype = sq.Column(sq.String, nullable=True) # FK?
    jobid = sq.Column(sq.String, nullable=False)
    complete = sq.Column(sq.Boolean, nullable=False)
    detector = sq.Column(sq.String, nullable=False) # FK?
    doorint = sq.Column(sq.String, nullable=True) # FK?
    doorext = sq.Column(sq.String, nullable=True) # FK?
    xposure = sq.Column(sq.Float, nullable=False)
    filter = sq.Column(sq.String, nullable=True) # FK?
    wavelnth = sq.Column(sq.Float, nullable=True) # FK?
    soopname = sq.Column(sq.String, nullable=True) # FK?
    sooptype = sq.Column(sq.String, nullable=True) # FK?
    obs_mode = sq.Column(sq.String, nullable=True) # FK?
    obs_id = sq.Column(sq.String, nullable=True)
    target = sq.Column(sq.String, nullable=True) # FK?
    pxbeg1 = sq.Column(sq.Integer, nullable=False)
    pxend1 = sq.Column(sq.Integer, nullable=False)
    pxbeg2 = sq.Column(sq.Integer, nullable=False)
    pxend2 = sq.Column(sq.Integer, nullable=False)
    nbin1 = sq.Column(sq.Integer, nullable=False)
    nbin2 = sq.Column(sq.Integer, nullable=False)
    detgainl = sq.Column(sq.Float, nullable=False)
    detgainh = sq.Column(sq.Float, nullable=False)
    gaincomb = sq.Column(sq.String, nullable=False) # FK?
    readoutm = sq.Column(sq.Integer, nullable=False)
    downloam = sq.Column(sq.Integer, nullable=False)
    gainthre = sq.Column(sq.Integer, nullable=True)
    ledstate = sq.Column(sq.String, nullable=False) # FK?
    ledvalue = sq.Column(sq.Integer, nullable=True)
    ledselec = sq.Column(sq.Integer, nullable=True)
    ledcontr = sq.Column(sq.String, nullable=True)
    tempint = sq.Column(sq.Float, nullable=False)
    temp1det = sq.Column(sq.Float, nullable=True)
    temp2det = sq.Column(sq.Float, nullable=True)
    ttemp1 = sq.Column(sq.DateTime, nullable=True)
    ttemp2 = sq.Column(sq.DateTime, nullable=True)
    # + detreg...
    dsun_au = sq.Column(sq.Float, nullable=True)

    def __repr__(self):
        return '<EuiFile(filename={}, xposure={})>'.format(self.filename, self.xposure)


class EuiFileError(Base):
    '''
    Files that have errors and cannot be ingested
    '''
    __tablename__ = 'eui_file_errors'
    id = sq.Column(sq.Integer, primary_key=True)
    filename = sq.Column(sq.String, unique=True, nullable=False)
    filepath = sq.Column(sq.String, nullable=False)

    def __repr__(self):
        return '<EuiFileError(filename={}, filepath={})>'.format(self.filename, self.filepath)


class DataBase:
    def __init__(self):
        self.connect()
        self.create_table()
        self.session = sq.orm.sessionmaker(bind=self.engine)()

    def connect(self):
        self.engine = sq.create_engine(parameters.db_uri, echo=True)

    def create_table(self):
        '''
        Create tables (if do not exist already)
        '''
        Base.metadata.create_all(self.engine)

    def close(self):
        self.session.close()
        self.engine.dispose()

    def __enter__(self):
        return self

    def __exit__(self):
        self.close()

    def scan_dir(self, directory='', level=2):
        '''
        Scan directory for EUI FITS files and insert their metadata
        to the database

        Parameters
        ----------
        directory: string
            Directory to scan (relative to parameters.data_path)
        level: int or str
            File level. "?" can be used for "all levels"

        Note: L0 files are not supported, and are skipped.
        '''
        files = glob.glob(
            os.path.join(
                parameters.data_path,
                directory,
                f"**/solo_L{level}_eui-*.fits"),
            recursive=True)
        for f in files:
            if "solo_L0_eui" in f:
                continue  # Skip L0 files
            self.insert_file(f[len(parameters.data_path):])

    def insert_file(self, f, filepath=None):
        '''
        Insert file metadata to database

        Parameters
        ----------
        f: string
            File name
        filepath: string
            File path (must be relative to `parameters.data_path`)

        File name can include path (if filepath is None), then this path
        should also be relative to `parameters.data_path`.
        '''
        if filepath is None:
            filepath, filename = os.path.split(f)
        else:
            filename = f
        fullname = os.path.join(parameters.data_path, filepath, filename)
        print(parameters.data_path, filepath, filename, fullname)
        n = self.session.query(EuiFile) \
            .filter_by(filename=filename) \
            .count()
        assert n <= 1
        if n == 1:   # row already exists. TODO: option to update existing rows?
            return
        hdulist = fits.open(fullname)
        fits_headers_to_read = list(EuiFile.__dict__.keys())
        fits_headers_to_read.append("date-beg")
        h = dict()
        for key, value in hdulist[0].header.items():
            key = key.lower()
            print(key, key in fits_headers_to_read, value)
            if key in fits_headers_to_read:
                if isinstance(value, fits.card.Undefined):
                    h[key] = None
                else:
                    h[key] = value
                print(f"Found {key}: {value}")
        hdulist.close()

        # transform metadata for the database
        h['filepath'] = filepath
        h['date_beg'] = h.get('date-beg')
        h.pop('date-beg')
        h['complete'] = (h['complete'] == 'C')
        for d in ['date', 'date_beg', 'ttemp1', 'ttemp2']:
            try:
                h[d] = fromisoformat(h[d])
            except KeyError:
                h[d] = None
        eui_file = EuiFile(**h)
        self.session.add(eui_file)
        self.session.commit()

    def count_files(self):
        return self.session.query(EuiFile).count()

    def to_csv(self, filename="/tmp/euifiles.csv", sep=","):
        """
        Export table to CSV

        Parameters
        ----------
        filename: str
            Output CSV file name
        sep: str
            CSV column separator
        """
        q = self.session.query(EuiFile)
        column_names = list(filter(
            lambda s: s[0] != "_",
            EuiFile.__dict__.keys()
            ))
        with open(filename, "w") as f:
            print(sep.join(column_names), file=f)
            for euifile in q.all():
                for column_name in column_names:
                    print(euifile.__dict__[column_name], end=sep, file=f)
                print(file=f)
