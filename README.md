# Solar Orbiter/EUI files database

## Installation

* Copy `parameters-template.py` to `parameters.py` and set paths for database and data archive.
* Run `pip install .`

## Usage

* Populate database: see `examples/populate-database.py`.
* Query: see `examples/query.py`.

